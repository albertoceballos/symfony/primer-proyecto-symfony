<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TaskType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
       $builder->add('title', TextType::class,[
           'label'=>'Título',
       ])
       ->add('content', TextareaType::class,[
           'label'=>'Contenido',
       ])
       ->add('priority', ChoiceType::class,[
           'label'=>'Prioridad',
           'choices'=>[
               'high'=>'alta',
               'medium'=>'media',
               'low'=>'baja',
           ]
       ])
       ->add('hours', TextType::class,[
           'label'=>'Horas presupuestadas',
       ])
       ->add('submit', SubmitType::class,[
           'label'=>'Crear',
       ]);
    }
    
}

