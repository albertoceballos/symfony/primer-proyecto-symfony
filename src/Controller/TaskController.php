<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

//cargar entidad

use App\Entity\Task;
use App\Entity\User;

//Cargar formulario

use App\Form\TaskType;

class TaskController extends AbstractController
{
    /**
     * @Route("/task", name="task")
     */
    public function index()
    {
        
        //Recorrer cada tarea junto a la tarea a la que pertenece:
        $task_repo=$this->getDoctrine()->getRepository(Task::class);
        
        $tasks=$task_repo->findBy([], ['id'=>'DESC']);
        
        /*foreach($tasks as $task){
            echo "Nombre de tarea: ".$task->getTitle().", Usuario: ". $task->getUser()->getName()."<br>";
            
        
        
        
        
        //Recorrer cada usuario con todas las tareas asignadas
        $user_repo= $this->getDoctrine()->getRepository(User::class);
        
        $users=$user_repo->findAll();
        
        foreach ($users as $user){
            
            echo "<h3>".$user->getName()." ".$user->getSurname()."</h3>";
            foreach ($user->getTasks() as $task){
                echo $task->getTitle()."<br>";
            }
        }*/
        
        return $this->render('task/index.html.twig', [
            'controller_name' => 'TaskController',
            'tasks'=>$tasks,
        ]);
    }
    
    public function detail(Task $task){
        
        if(!$task){
            
            return $this->redirectToRoute('tasks');
        }else{
            
            return $this->render('task/detail.html.twig',[
                'task'=>$task,
            ]);
        }
    }
    
    public function creation(Request $request, UserInterface  $user){
        
        //crear un obejeto task, creo el formulario y le paso el objeto $task para rellenar los datos
        $task=new Task();
        $form= $this->createForm(TaskType::class,$task);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
            $createdAt=(new \DateTime('now'))->format('Y-m-d H:i:s');
            $task->setCreatedAt($createdAt);
            $task->setUser($user);
            
            $em= $this->getDoctrine()->getManager();
            
            $em->persist($task);
            $em->flush();
            
            return $this->redirect($this->generateUrl('task_detail',['id'=>$task->getId()]));
        }
        
        return $this->render('task/creation.html.twig',[
            'form'=>$form->createView(),
        ]);
    }
    
    public function myTasks(UserInterface $user){
        
        $tasks=$user->getTasks();
        return $this->render('task/my-tasks.html.twig',[
            'tasks'=>$tasks,
        ]);
        
    }
    
    public function edit(Request $request, UserInterface $user, Task $task){
        //editar Tarea
        
        if(!$user || $user->getId() != $task->getUser()->getId()){
            return $this->redirect('tasks');
                   
        }
        $form= $this->createForm(TaskType::class,$task);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
            $em= $this->getDoctrine()->getManager();
            
            $em->persist($task);
            $em->flush();
            
            return $this->redirect($this->generateUrl('task_detail',['id'=>$task->getId()]));
        }
       
        
        return $this->render('task/creation.html.twig',[
           'form'=>$form->createView(),
           'edit'=>true,
        ]);
    }
    
    public function delete(Task $task, UserInterface $user){
         if(!$user || $user->getId() != $task->getUser()->getId()){
            return $this->redirect('tasks');
                   
        }
        
        if(!$task){
            return $this->redirectToRoute('tasks');
        }
        
        $em= $this->getDoctrine()->getManager();
        $em->remove($task);
        $em->flush();
     
       return $this->redirectToRoute('tasks');   
    }
    
}
