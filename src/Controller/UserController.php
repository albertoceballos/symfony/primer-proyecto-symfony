<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


use Symfony\Component\HttpFoundation\Request;

//Cargar el encoder
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

//importar la entidad
use App\Entity\User;

//importar clase del formulario creado
use App\Form\RegisterType;


//Libreria para la autenticación
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;



class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }
    
    public function register(Request $request, UserPasswordEncoderInterface $encoder){
        
        //crear el formulario
        $user=new User();
        $form= $this->createForm(RegisterType::class,$user);
        
        //Rellenar el objeto con los datos del form
        $form->handleRequest($request);
        
        //comprobar si el form se ha enviado
        if($form->isSubmitted() && $form->isValid()){
          
            $user->setRole('USER_ROLE');
            
            //establecer el createdAt
            $user->setCreatedAt(new \DateTime('now'));
            
            //cifrar contraseña
            $encoded=$encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded);
            var_dump($user);
            
            //Guardar el usuario
            $entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            
            return $this->redirectToRoute('register');
        }
        
        
        return $this->render('user/register.html.twig',['formulario'=>$form->createView()]);
        
    }
    
    public function login(AuthenticationUtils $authtenticationUtils){
        //variable en la que guardamos el error en caso de que lo haya
        $error=$authtenticationUtils->getLastAuthenticationError();
        
        //variable para guardar el nombre del usuario que intentó autenticarse y ha fallado
        $lastUserName=$authtenticationUtils->getLastUsername();
        
        return $this->render('user/login.html.twig',[
            'error'=>$error,
            'last_username'=>$lastUserName,
        ]);
    }
    
}
